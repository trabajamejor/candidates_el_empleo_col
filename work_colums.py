# To add a new cell, type '#   '
# To add a new markdown cell, type '#    [markdown]'
#   
import re
import gc
import pandas as pd
import numpy as np
import pymysql
import datetime

#   
# Import user def functions
from utils.queries  import write_query_work
from utils.clean_work import  get_work_duration, get_work_time


def get_work_columns_df(con, candidates_id_list) -> pd.DataFrame:

    #   


    #   
    df_work = pd.read_sql_query(sql=write_query_work(candidates_id_list), con=con)
    # Convert each column to a specific dtype    example: object -> string   
    df_work = df_work.convert_dtypes()
    # Set user_id as index
    df_work.set_index("user_id", inplace=True)
    df_work.sort_index(inplace=True)


    #   
    #Get duración_trabajo column
    df_work["duracion_trabajo"] = df_work.apply(lambda df: get_work_duration(df["fecha_inicio"], df["fecha_fin"]), axis=1)

    #Get tiempo_trabajo column
    df_work["tiempo_trabajo"] = df_work.apply(lambda df: get_work_time(df["fecha_inicio"], df["fecha_fin"]), axis=1)

    #  Replace end_Date  ("0001-01-01") with Today 
    df_work.loc[df_work.fecha_fin == datetime.date(1, 1, 1), "fecha_fin"] = datetime.datetime.now().strftime('%Y-%m-%d')

    # Sort by user and end_date to keeep the firts five studies 
    df_work.sort_values(['user_id', 'fecha_fin'],
                        ascending=[True, False],
                        inplace=True)

    # Drop Columns 
    df_work.drop(columns=["fecha_inicio","fecha_fin"], inplace=True)


    #   
    duracion_trabajo = []
    lugar_trabajo = []
    nombre_trabajo = []
    tiempo_trabajo = []
    descripcion_trabajo = []
    for ii in range(1, 6):
        duracion_trabajo.append(f"duracion_trabajo{ii}")
        lugar_trabajo.append(f"lugar_trabajo{ii}")
        nombre_trabajo.append(f"nombre_trabajo{ii}")
        tiempo_trabajo.append(f"tiempo_trabajo{ii}")
        descripcion_trabajo.append(f"descripcion_trabajo{ii}")

    cols_user_work = ["user_id"] + duracion_trabajo + lugar_trabajo + nombre_trabajo + tiempo_trabajo + descripcion_trabajo


    # Get list of dict of five firts educations
    last_index = df_work.index[0]
    list_of_user_dict = []
    count_work = 0
    work_user_dict = {}
    work_user_dict["user_id"] = last_index
    for index, rows in df_work.iterrows():

        if index == last_index: 
            count_work +=1
        else:
            list_of_user_dict.append(work_user_dict)
            count_work = 1
            work_user_dict = {}
            work_user_dict["user_id"] = index

        if count_work <= 5:
            work_user_dict[f"duracion_trabajo{count_work}"] = rows.duracion_trabajo
            work_user_dict[f"lugar_trabajo{count_work}"] = rows.lugar_trabajo
            work_user_dict[f"nombre_trabajo{count_work}"] = rows.nombre_trabajo
            work_user_dict[f"tiempo_trabajo{count_work}"] = rows.tiempo_trabajo
            work_user_dict[f"descripcion_trabajo{count_work}"] = rows.descripcion_trabajo
            
        last_index = index

    list_of_user_dict.append(work_user_dict)


    #   
    df_users_works = pd.DataFrame(list_of_user_dict, columns=cols_user_work)
    df_users_works.set_index("user_id",inplace=True)
    df_users_works = df_users_works.convert_dtypes()


    return df_users_works