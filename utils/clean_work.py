"""
User def Fuctions to clean data
"""
import datetime
import locale

_MOTHS_DICT = {
    "January": "Enero",
    "February": "Febrero",
    "March": "Marzo",
    "April": "Abril",
    "May": "Mayo",
    "June": "Junio",
    "July": "Julio",
    "August": "Agosto",
    "September": "Septiembre",
    "October": "Octubre",
    "November": "Noviembre",
    "December": "Diciembre",
}

def get_work_duration(start_date: datetime.date, end_date: datetime.date) -> str:
    """
    get duracion_trabajo format
    """
    if end_date == datetime.date(1, 1, 1):
        duration = (datetime.date.today()-start_date).days
    else:
        duration = (end_date-start_date).days

    years = (duration // 365.25)
    months = (duration // 30.43)

    if years > 0 :
        if months > 0:
            return  f"{int(years)} año y {int(months-years*12)} meses"
        return f"{int(months)} meses"
    return f"{int(months)} meses"


def replace_words(my_string, words):
    """
    replace months from english to spanish
    """
    for keys, values in words.items():
        my_string = my_string.replace(keys, values)
    return my_string

def get_work_time(start_date: datetime.date, end_date: datetime.date) -> str:
    """
    get work_time format
    """

    if end_date == datetime.date(1, 1, 1):
        date = start_date.strftime('%B de %Y')
        date = replace_words(date, _MOTHS_DICT).capitalize()
        return f"{date} - Actualmente"

    date_start = start_date.strftime('%B de %Y')
    date_start = replace_words(date_start, _MOTHS_DICT).capitalize()

    date_end = end_date.strftime('%B de %Y')
    date_end = replace_words(date_end, _MOTHS_DICT).capitalize()
    return f"{date_start} - {date_end}"
