"""
Read a file from a google SPREADSHEET
"""
import os
import pickle
import pandas as pd
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from dotenv import load_dotenv
from pathlib import Path

dotenv_path = Path('/var/www/html/.env')
load_dotenv(dotenv_path=dotenv_path)

SCOPES_ROUTE = os.getenv('FILES_SCOPE')

# here enter the id of your google sheets
SCOPES = [SCOPES_ROUTE]
SAMPLE_RANGE_NAME = 'A1:AA10000'


def read_docs() -> pd.DataFrame:
    """
    get a pandas dataframe from a google spreadsheets
    """
    credentials_token = os.getenv('CREDENTIALS_TOKEN')
    credentials_json = os.getenv('CREDENTIALS')
    EL_EMPLEO = os.getenv('EL_EMPLEO')
    creds = None
    if os.path.exists(credentials_token):
        with open(credentials_token, 'rb') as token:
            creds = pickle.load(token)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(credentials_json, SCOPES)
            # here enter the name of your downloaded JSON file
            creds = flow.run_local_server(port=0)
        with open(credentials_token, 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result_input = sheet.values().get(spreadsheetId=EL_EMPLEO,
                                      range=SAMPLE_RANGE_NAME).execute()
    values_input = result_input.get('values', [])

    if not values_input and not values_expansion:
        print('No data found.')

    return pd.DataFrame(values_input[1:], columns=values_input[0])
