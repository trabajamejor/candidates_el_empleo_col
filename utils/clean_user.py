"""
User def Fuctions to clean data
"""
import datetime
import locale
import re
import pandas as pd
from services.consultas import get_dictionary



_MOTHS_DICT = {
    "January": "Enero",
    "February": "Febrero",
    "March": "Marzo",
    "April": "Abril",
    "May": "Mayo",
    "June": "Junio",
    "July": "Julio",
    "August": "Agosto",
    "September": "Septiembre",
    "October": "Octubre",
    "November": "Noviembre",
    "December": "Diciembre",
}

def get_salary_dict(salary_colum:pd.Series) -> dict:
    """
    get salary dict to replace ranges for str value
    """
    unique_salaries = salary_colum.unique()
    salary_replace_ranges = {}
    for salary in unique_salaries:
        ranges=salary.replace("$","").replace(",",".").split(" a ")
        if len(ranges) > 1:
            salary_replace_ranges[salary] = int((float(ranges[0]) + float(ranges[1])) * 1000000/2)
    salary_bounds = get_dictionary("elempleo_salary_bounds")

    salary_replace_ranges.update(salary_bounds)

    return salary_replace_ranges


def get_phone_mumber(number:str) -> str:
    """
    get phone number as string
    """
    if isinstance(number, str):
        number=re.sub(r"[^0-9.]","",number)
        if len(number)>10:
            return number[-10:]
        return number
    return ""


def get_english_level(percent:str) -> str:
    """
    change english porcent for a valie
    """
    percent = int(percent)
    if percent <= 20:
        return "(MuyBásico)"
    if 20 < percent <= 40:
        return "(Básico)"
    if 40 < percent <= 60:
        return "(Intermedio)"
    if 60 < percent <= 90:
        return "(Avanzado)"
    if percent > 90:
        return "(Nativo)"
    return ""

def replace_words(my_string, words):
    """
    replace months from english to spanish
    """
    for keys, values in words.items():
        my_string = my_string.replace(keys, values)
    return my_string

def get_ultimo_login_format(date:datetime.date) -> str:
    """
    get ultimo login format
    """
    date = date.strftime('%d %B de %Y')
    date = replace_words(date, _MOTHS_DICT)
    return f"{date}"
