"""
clean data funtions
"""

def get_offer_code(url_list:list):
    """
    split el empleo  url to get offer code
    """
    return list(map(url_split,url_list))


def url_split(url:str):
    """
    split el empleo  url to get offer code
    """

    return url.split("/")[-1]

def url_get_code_offer(url:str):
    """
    split el empleo  url to get offer code
    """
    return url.split("/")[-1].strip()
