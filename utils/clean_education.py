"""
User def Fuctions to clean data
"""

import datetime
import locale
from services.consultas import get_dictionary

_MOTHS_DICT = {
    "January": "Enero",
    "February": "Febrero",
    "March": "Marzo",
    "April": "Abril",
    "May": "Mayo",
    "June": "Junio",
    "July": "Julio",
    "August": "Agosto",
    "September": "Septiembre",
    "October": "Octubre",
    "November": "Noviembre",
    "December": "Diciembre",
}

def status_education_dict() -> dict:
    """
    get dict to replace status education
    """
    status = get_dictionary("elempleo_education_status")
    return status


def nivel_education_dict() -> dict:
    """
    get dict to replace Level education
    """
    level = get_dictionary("elempleo_education_levels")
    return level

def replace_words(my_string, words):
    """
    replace months from english to spanish
    """
    for keys, values in words.items():
        my_string = my_string.replace(keys, values)
    return my_string

def get_time_formation(start_date:datetime.date, end_date:datetime.date) -> str:
    """
    get tiempo_formacion fortmat
    """
    

    if end_date == datetime.date(1, 1, 1):
        date = start_date.strftime('%B de %Y')
        date = replace_words(date, _MOTHS_DICT).capitalize()
        return f"Iniciado en: {date}"

    date_start = start_date.strftime('%B de %Y')
    date_start = replace_words(date_start, _MOTHS_DICT).capitalize()

    date_end = end_date.strftime('%B de %Y')
    date_end = replace_words(date_end, _MOTHS_DICT).capitalize()
    return f"{date_start} - {date_end}"
