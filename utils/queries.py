"""
Queries Static
"""


def write_query_user(offers_code_url):
    QUERY_USER = f"""
        SELECT
            `users`.`id` as `user_id`,
            `users`.`identification` as `cedula`,
            `users`.`email` as `correo`,
            `users`.`profile` as `descripcion`,
            `users`.`can_relocate` as `disponibilidad_para_cambio_de_residencia`,
            `users`.`can_travel` as `disponibilidad_para_viajar` ,
            FLOOR (DATEDIFF (NOW(),`users`.`birth_date`)/365.25) as `edad`,
            `marital_statuses`.`name` as `estado_civil`,
            CONCAT_WS('/',`users`.`department`, `users`.`city`) as `localizacion`,
            `users`.`salary_range` as `neto_mensual` ,
            CONCAT_WS(' ',`users`.`name`, `users`.`first_surname`,`users`.`second_surname`) as `nombre`,
            `users`.`has_work` as `situacion_laboral`,
            `users`.`phone` as `telefono_1`,
            `users`.`cellphone` as `telefono_2`,
            `users`.`created_at` as `tiempo` ,
            '' as `vehiculo_propio`,
            `company_job_offers`.`name` as  `nombre_oferta`,        
            CONCAT_WS('-',`users`.`id`, `company_job_offers`.`offer_code_url`) as `link`,
            DATE_FORMAT(`user_applied_offers`.`offer_application_date`, "%Y-%m-%d") as `fecha_aplicacion`,
            `users`.`updated_at` as `ultima_modificacion` ,
            CONCAT_WS('/','https://www.elempleo.com/co/empresas/buscar/oferta',`company_job_offers`.`offer_code_url`) as `url_oferta`,
            `company_job_offers`.`offer_code_url` as `id_source_offer`
        FROM `prod_eltiempo_api`.`users`
        left join  `prod_eltiempo_api`.`marital_statuses`
        ON  `users`.`marital_status_id` = `marital_statuses`.`id`
        left join  `prod_eltiempo_api`. `user_applied_offers`
        ON `users`.`id` = `user_applied_offers`.`user_id`
        left join  `prod_eltiempo_api`. `company_job_offers`
        ON `user_applied_offers`.`company_job_offer_id` = `company_job_offers`.`id`
        WHERE `offer_code_url` in ({', '.join(offers_code_url)});
    """
    return QUERY_USER

def write_query_languages(candidates_id_list):

    QUERY_LANGUAGES = f"""
        SELECT 
            `languages`.`id`,
            `languages`.`user_id`,
            `languages`.`name`,
            `languages`.`percent`
        FROM `prod_eltiempo_api`.`languages`
        WHERE `languages`.`name` != "(Otro)" AND `languages`.`user_id`in ({', '.join(candidates_id_list)});
    """

    return QUERY_LANGUAGES



def write_query_education(candidates_id_list):  

    QUERY_EDUCATION = f"""
        SELECT
            `formal_educations`.`user_id`,
            `formal_educations`.`title` as  `area_formacion`, 
            `formal_educations`.`Level_name` as `nivel`,
            `formal_education_states`.`name` as  `estado`,
            `formal_educations`.`institute_name` as  `lugar_formacion`,
            `formal_educations`.`date_from` as 'fecha_inicio',
            `formal_educations`.`date_to` as `fecha_fin`
        FROM `prod_eltiempo_api`.`formal_educations`
        left join  `prod_eltiempo_api`.`formal_education_states`
        ON  `formal_educations`.`formal_education_state_id` = `formal_education_states`.`id`
        WHERE  `formal_educations`.`Level_name` != "Preescolar" and `formal_educations`.`user_id` in ({', '.join(candidates_id_list)});
    """
    return QUERY_EDUCATION

def write_query_work(candidates_id_list):

    QUERY_WORK = f"""
        SELECT 
            `experiences`.`user_id`,
            `experiences`.`function` as `descripcion_trabajo`,
            `experiences`.`date_from` as `fecha_inicio`,
            `experiences`.`date_to` as `fecha_fin`,
            CONCAT_WS(' / ',`experiences`.`company_name`, `experiences`.`area_name`,`experiences`.`sub_sector`) as `lugar_trabajo`,
            `experiences`.`posission_name` as `nombre_trabajo`
        FROM `prod_eltiempo_api`.`experiences`
        WHERE `experiences`.`user_id` in ({', '.join(candidates_id_list)});
    """
    
    return QUERY_WORK