"""
main file to get el empleo candidates data
"""
from datetime import datetime
import pandas as pd
import numpy as np
import pymysql
import os
from sqlalchemy import create_engine
from studies_colums import get_studies_columns_df
from work_colums import get_work_columns_df
from utils.queries import write_query_user, write_query_languages
from utils.clean_user import get_salary_dict, get_phone_mumber, get_english_level, get_ultimo_login_format
from utils.read_google_docs import read_docs
from utils.clean_data import get_offer_code, url_get_code_offer
from dotenv import load_dotenv
from pathlib import Path

dotenv_path = Path('/var/www/html/.env')
load_dotenv(dotenv_path=dotenv_path)


def update_offers_urls_table(df_urls_empleo: pd.DataFrame):
    """
    update url_offers table at db
    """
    tiempo_DB = os.getenv('tiempo_DB')
    tiempo_DB_HOST = os.getenv('tiempo_DB_HOST')
    tiempo_DB_USER = os.getenv('tiempo_DB_USER')
    tiempo_DB_PASSWD = os.getenv('tiempo_DB_PASSWD')
    tiempo_DB_PORT = os.getenv('tiempo_DB_PORT')

    engine = create_engine(
        'mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8'.format(tiempo_DB_USER, tiempo_DB_PASSWD, tiempo_DB_HOST,
                                                             tiempo_DB_PORT, tiempo_DB),
        encoding='utf8', pool_recycle=3600
    )
    df_url_offers = pd.DataFrame(columns=["url", "offer_code", "created_at", "updated_at"])
    df_url_offers["url"] = df_urls_empleo["URL"]
    df_url_offers["offer_code"] = df_urls_empleo["URL"].apply(url_get_code_offer)
    df_url_offers["created_at"] = f"{datetime.today().strftime('%Y-%m-%d_%H:%M:%S')}"
    df_url_offers["updated_at"] = f"{datetime.today().strftime('%Y-%m-%d_%H:%M:%S')}"

    df_url_offers.drop_duplicates(subset=["offer_code"],
                                  keep='last',
                                  inplace=True)

    df_url_offers.to_sql("url_offers",
                         con=engine,
                         if_exists="replace",
                         chunksize=15000,
                         index_label="id")

    print(f"url_offers tables updated with {len(df_url_offers)} offers from Google Sheets")


def get_code_offers_list() -> list:
    """
    get active code offers from google sheet
    """
    df_urls_empleo = read_docs()

    df_urls_empleo["URL"] = df_urls_empleo["URL"].str.strip()
    df_urls_empleo["URL"] = df_urls_empleo["URL"].astype(str)
    df_urls_empleo["Talentu Web"] = df_urls_empleo["Talentu Web"].str.lower().str.strip()
    df_urls_empleo["Talentu Web"] = df_urls_empleo["Talentu Web"].astype(str)

    update_offers_urls_table(df_urls_empleo=df_urls_empleo)

    df_talentu = df_urls_empleo[df_urls_empleo["Talentu Web"].isin(["si", "sí", "Si", "yes", "Sí"])]
    code_offers_list = get_offer_code(df_talentu["URL"].tolist())
    return list(filter(None, code_offers_list))


def run_el_empleo_get_data(offers_code_url: list):
    """
    get data from DB
    """
    tiempo_DB = os.getenv('tiempo_DB')
    tiempo_DB_HOST = os.getenv('tiempo_DB_HOST')
    tiempo_DB_USER = os.getenv('tiempo_DB_USER')
    tiempo_DB_PASSWD = os.getenv('tiempo_DB_PASSWD')
    Ruta_temporal = os.getenv('Ruta_temporal')
    conn = pymysql.connect(port=3306,
                           host=tiempo_DB_HOST,
                           user=tiempo_DB_USER,
                           password=tiempo_DB_PASSWD,
                           db=tiempo_DB,
                           charset='utf8')

    # get basic user information
    df_users = pd.read_sql_query(sql=write_query_user(offers_code_url=offers_code_url), con=conn)
    # Convert each column to a specific dtype    example: object -> string
    df_users = df_users.convert_dtypes()

    df_users.set_index("user_id", inplace=True)
    df_users.sort_index(inplace=True)

    df_users["disponibilidad_para_cambio_de_residencia"] = pd.Series(
        np.where(df_users.disponibilidad_para_cambio_de_residencia.values == 1, "Sí", "No"),
        df_users.index).convert_dtypes()
    df_users["disponibilidad_para_viajar"] = pd.Series(
        np.where(df_users.disponibilidad_para_viajar.values == 1, "Sí", "No"), df_users.index).convert_dtypes()
    df_users["situacion_laboral"] = pd.Series(np.where(df_users.situacion_laboral.values == 0, "", "No tengo empleo"),
                                              df_users.index).convert_dtypes()

    salary_dict = get_salary_dict(df_users["neto_mensual"])
    df_users.replace({"neto_mensual": salary_dict}, inplace=True)

    # get phone number
    tel_number_cols = ["telefono_1", "telefono_2"]
    for column in tel_number_cols:
        df_users[column] = df_users[column].apply(get_phone_mumber)

    df_users["ultima_modificacion"] = df_users["ultima_modificacion"].apply(get_ultimo_login_format)

    candidates_id_list = list(map(str, list(df_users.index.values)))

    # GET LANGUAGES
    df_languages = pd.read_sql_query(sql=write_query_languages(candidates_id_list), con=conn)
    # Convert each column to a specific dtype    example: object -> string
    df_languages = df_languages.convert_dtypes()
    # Convert percent as Categorical
    df_languages["percent"] = df_languages["percent"].apply(get_english_level).convert_dtypes()
    # Concatenate name and level in one column
    df_languages["languages"] = df_languages["name"] + df_languages["percent"]
    # Group by user Id an join with -
    languages_serie = df_languages.groupby(["user_id"])["languages"].agg(lambda x: '-'.join(x.astype(str)))
    df_users["languages"] = languages_serie
    # Get Work experience Columns
    df_users_work = get_work_columns_df(con=conn, candidates_id_list=candidates_id_list)

    # Get Studies
    df_users_studies = get_studies_columns_df(con=conn, candidates_id_list=candidates_id_list)

    data = pd.concat([df_users, df_users_work, df_users_studies], axis=1)

    conn.close()

    data["descripcion_breve"] = "0"
    data["fecha_registro"] = "1 de enero de 1999"
    data["hoja_de_vida"] = "No adjunto hoja de vida"
    data["licencia_conduccion"] = ""
    data["preguntas_filtro1_pregunta"] = "0"
    data["preguntas_filtro1_respuesta"] = "0"
    data["preguntas_filtro2_pregunta"] = "0"
    data["preguntas_filtro2_respuesta"] = "0"
    data["preguntas_filtro3_pregunta"] = "0"
    data["preguntas_filtro3_respuesta"] = "0"
    data["preguntas_filtro4_pregunta"] = "0"
    data["preguntas_filtro4_respuesta"] = "0"
    data["preguntas_filtro5_pregunta"] = "0"
    data["preguntas_filtro5_respuesta"] = "0"
    data["ultimo_login"] = data["ultima_modificacion"]
    data["vehiculo_propio"] = ""
    data["nombre_habilidad"] = ""
    data["cuerpo_habilidad"] = ""
    data["adecuacion"] = ""
    data["source"] = "El Empleo"

    data["fecha_aplicacion"] = data["fecha_aplicacion"].astype(str)
    data["ultimo_login"] = data["ultimo_login"].astype(str)
    data["ultima_modificacion"] = data["ultima_modificacion"].astype(str)
    data["tiempo"] = data["tiempo"].astype(str)
    data.dropna(subset=["cedula"], inplace=True)

    date_now = datetime.today().strftime('%Y-%m-%d_%H-%M-%S')

    data.to_csv(f"/{Ruta_temporal}/df_candidates_elempleo_CO_{date_now}.csv",
                encoding="utf-8-sig",
                date_format="Y-%m-%d",
                index=False)

    print(f"Candidates from el Empleo: {len(data)}")


if __name__ == "__main__":
    date_now = datetime.today().strftime('%Y-%m-%d_%H-%M-%S')
    print(f"El empleo candidates starts at: {date_now}")
    # Get offers code from  google sheet
    offers_code_url = get_code_offers_list()
    print(f"Offer code list  with Talentu Web: {offers_code_url}")
    # Get Candidates info from DB
    run_el_empleo_get_data(offers_code_url)
    date_now = datetime.today().strftime('%Y-%m-%d_%H-%M-%S')
    print(f"El empleo candidates ends at: {date_now}")
