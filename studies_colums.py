"""
Study Clean Data
"""
import pandas as pd
import pymysql
import datetime
from utils.queries  import write_query_education
from utils.clean_education import  status_education_dict, nivel_education_dict, get_time_formation

def get_studies_columns_df(con, candidates_id_list) -> pd.DataFrame:
    """
        Return colums to form df studies
    """
    # GET EDUCATION DF from formal education table
    df_education = pd.read_sql_query(sql=write_query_education(candidates_id_list), con=con)
    # Convert each column to a specific dtype    example: object -> string
    df_education = df_education.convert_dtypes()

    # Set user_id as index
    df_education.set_index("user_id", inplace=True)
    df_education.sort_index(inplace=True)

    #  
    df_education.replace({"estado": status_education_dict(),
                        "nivel": nivel_education_dict()}, inplace=True)

    #  
    df_education["descripcion_formacion"] = df_education["nivel"] + "-" + df_education["estado"]
    df_education.drop(columns=["nivel", "estado"], inplace=True)


    #  
    #df_education = df_education.convert_dtypes()

    #  
    #Get tiempo_formación column

    df_education["tiempo_formacion"] = df_education.apply(lambda df: get_time_formation(df["fecha_inicio"], df["fecha_fin"]), axis=1)

    #  Replace end_Date  ("0001-01-01") with Today 
    df_education.loc[df_education.fecha_fin == datetime.date(1, 1, 1), "fecha_fin"] = datetime.datetime.now().strftime('%Y-%m-%d')

    # Sort by user and end_date to keeep the firts five studies 
    df_education.sort_values(['user_id', 'fecha_fin'],
                            ascending=[True, False],
                            inplace=True)

    # Drop Columns 
    df_education.drop(columns=["fecha_inicio","fecha_fin"], inplace=True)


    #  
    area_formacion = []
    descripcion_formacion = []
    lugar_formacion = []
    tiempo_formacion = []
    
    for ii in range(1,6):
        area_formacion.append(f"area_formacion{ii}")
        descripcion_formacion.append(f"descripcion_formacion{ii}")
        lugar_formacion.append(f"lugar_formacion{ii}")
        tiempo_formacion.append(f"tiempo_formacion{ii}")
        
    cols_user_education = ["user_id"] + area_formacion + descripcion_formacion + lugar_formacion + tiempo_formacion


    #  
    # Get list of dict of five firts educations
    last_index = df_education.index[0]
    list_of_user_dict = []
    count_study = 0
    study_user_dict = {}
    study_user_dict["user_id"] = last_index
    for index, rows in df_education.iterrows():

        if index == last_index: 
            count_study +=1
        else: 
            list_of_user_dict.append(study_user_dict)
            count_study = 1
            study_user_dict = {}
            study_user_dict["user_id"] = index

        if count_study <= 5:
            study_user_dict[f"area_formacion{count_study}"] = rows.area_formacion
            study_user_dict[f"lugar_formacion{count_study}"] = rows.lugar_formacion
            study_user_dict[f"descripcion_formacion{count_study}"] = rows.descripcion_formacion
            study_user_dict[f"tiempo_formacion{count_study}"] = rows.tiempo_formacion
            
        last_index = index

    list_of_user_dict.append(study_user_dict)

    df_users_education = pd.DataFrame(list_of_user_dict, columns=cols_user_education)
    df_users_education.set_index("user_id",inplace=True)
    df_users_education = df_users_education.convert_dtypes()

    return df_users_education
