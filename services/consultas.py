from services.db_utils import Database


def correct_value_type(x: str) -> any:
    try:
        return int(x)
    except:
        return x


def adjust_dict_value_types(dictionary: dict) -> dict:
    for keys in dictionary:
        dictionary[keys] = correct_value_type(dictionary[keys])
    return dictionary


def get_dictionary(group: str) -> dict:
    conn = Database().connect()
    cursor = conn.cursor()
    str_query = """select keyword, value from tbl_dictionaries where `groups` = %s"""
    cursor.execute(query=str_query, args=(group,))
    records = cursor.fetchall()
    records_sql = dict(records)
    records_sql = adjust_dict_value_types(records_sql)
    return records_sql
