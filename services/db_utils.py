from typing import Optional

import pymysql
import logging
from datetime import date, datetime
from dotenv import load_dotenv
from os import getenv
from pathlib import Path

dotenv_path = Path('/var/www/html/.env')
load_dotenv(dotenv_path=dotenv_path)

ANALYTICS_DB_USER = getenv('ANALYTICS_DB_USER')
ANALYTICS_DB_PASSWD = getenv('ANALYTICS_DB_PASSWD')
ANALYTICS_DB_HOST = getenv('ANALYTICS_DB_HOST')
ANALYTICS_DB_PORT = getenv('ANALYTICS_DB_PORT')
ANALYTICS_DB = getenv('ANALYTICS_DB')


class times:
    times = datetime.now().strftime('%Y-%m-%d %H:%M:%S')


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Database(metaclass=Singleton):
    connection: Optional[pymysql.connections.Connection] = None

    def connect(self) -> pymysql.connections.Connection:
        try:
            if self.connection is None:
                self.connection = pymysql.connect(host=ANALYTICS_DB_HOST, port=int(ANALYTICS_DB_PORT),
                                                  user=ANALYTICS_DB_USER, passwd=ANALYTICS_DB_PASSWD, db=ANALYTICS_DB)

        except pymysql.InterfaceError as e:
            print('{}: Unable to connect! {}'.format(times, e))
            logging.error('{}: Unable to connect!'.format(times, e))
        except pymysql.OperationalError as e:
            print("{}: Can't connect, check session data! {}".format(times, e))
            logging.error("{}: Can't connect, check session data! {}".format(times, e))
        except pymysql.ProgrammingError as e:
            print("{}: Syntax error in SQL statement or table not found or already exists {} ".format(times, e))
            logging.error("{}: Syntax error in SQL statement or table not found or already exists".format(times, e))
        except pymysql.InternalError as e:
            print("{}: Syntax error in SQL statement or table not found or already exists {} ".format(times, e))
            logging.error("{}: Syntax error in SQL statement or table not found or already exists".format(times, e))
        return self.connection
